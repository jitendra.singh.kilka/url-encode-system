<?php


namespace App\Models;


use Jenssegers\Mongodb\Eloquent\Model as Eloquent;


class Url extends Eloquent
{
	protected $connection = 'mongodb';
	protected $collection = 'urls';

    protected $fillable = [
        'encoded_url',
        'short_url',
        'expriration_at',
        'used_count'
    ];
}
