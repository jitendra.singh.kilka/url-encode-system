<?php


namespace App\Models;


use Jenssegers\Mongodb\Eloquent\Model as Eloquent;


class UrlConfig extends Eloquent
{
	protected $connection = 'mongodb';
	protected $collection = 'url_configs';

    protected $fillable = [
        'expriration_days',
        'used_count_limit'
    ];

    public function setExprirationDaysAttribute($value)
    {
        $this->attributes['expriration_days'] = (int) $value;
    }
    public function setUsedCountLimitAttribute($value)
    {
        $this->attributes['used_count_limit'] = (int) $value;
    }
}
