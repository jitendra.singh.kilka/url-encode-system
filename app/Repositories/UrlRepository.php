<?php

namespace App\Repositories;

use App\Models\Url;
use App\Models\UrlConfig;
use App\Services\UrlService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Crypt;

class UrlRepository
{
    public function UrlEncode($data)
    {
        $url = UrlService::base64url_encode(Crypt::encryptString($data['data']['url']));
        $short_url = UrlService::generateRandomUrl($url);
        $url_config = UrlService::getConfig();
        $url_obj = Url::create([
            'encoded_url' => $url,
            'short_url' => $short_url,
            'expriration_at' => Carbon::now()->addDays($url_config['expriration_days'])->format('Y-m-d H:i:s'),
            'used_count' => 0
        ]);
        return ['short_url' => $url_obj->short_url];
    }
    public function UrlDecode($data)
    {
        $url_obj = Url::where([
            'short_url' => $data['data']['short_url']
        ])->first();
        $url = Crypt::decryptString(UrlService::base64url_decode($url_obj->encoded_url));
        $url_obj->used_count += 1;
        $url_obj->save();
        return ['url' => $url];
    }
    public function SetConfig($data)
    {
        UrlConfig::updateOrCreate([], [
            'expriration_days' => $data['data']['expriration_days'],
            'used_count_limit' => $data['data']['used_count_limit']
        ]);
    }
    public function renderShortUrl($data){
        return $this->UrlDecode($data);
    }
    public function GetConfig($data){
        return UrlService::getConfig();
    }
}
