<?php

namespace App\Services;

use App\Models\Url;
use App\Models\UrlConfig;

class UrlService
{
    public static function base64url_encode($url)
    {
        return rtrim(strtr(base64_encode($url), '+/', '-_'), '=');
    }

    public static function base64url_decode($url)
    {
        return base64_decode(str_pad(strtr($url, '-_', '+/'), strlen($url) % 4, '=', STR_PAD_RIGHT));
    }

    public static function generateRandomUrl($url, $length = 7)
    {
        $urlLength = strlen($url);
        $randomurl = '';
        for ($i = 0; $i < $length; $i++) {
            $randomurl .= $url[rand(0, $urlLength - 1)];
        }
        while (self::checkShortUrl($randomurl)) {
            $length++;
            self::generateRandomUrl($url, $length);
        }
        return $randomurl;
    }
    public static function getConfig()
    {
        $config = UrlConfig::first();
        if ($config) {
            return ['expriration_days' => $config->expriration_days, 'used_count_limit' => $config->used_count_limit];
        } else {
            return ['expriration_days' => 2, 'used_count_limit' => 10];
        }
    }
    public static function checkShortUrl($short_url)
    {
        return Url::where('short_url', '=', $short_url)->exists();
    }
}
