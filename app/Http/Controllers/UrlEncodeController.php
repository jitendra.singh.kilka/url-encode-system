<?php

namespace App\Http\Controllers;

use App\Http\Requests\DecodeRequest;
use App\Http\Requests\EncodeRequest;
use App\Http\Requests\RedirectRequest;
use App\Http\Requests\SetConfigRequest;
use App\Repositories\UrlRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class UrlEncodeController extends BaseController
{
    protected $repository = UrlRepository::class;

    public function UrlEncode(EncodeRequest $request)
    {
        $this->jobMethod = 'UrlEncode';
        return $this->handleCustomEndPoint($request);
    }
    public function UrlDecode(DecodeRequest $request)
    {
        $this->jobMethod = 'UrlDecode';
        return $this->handleCustomEndPoint($request);
    }
    public function SetConfig(SetConfigRequest $request)
    {
        $this->jobMethod = 'SetConfig';
        return $this->handleCustomEndPoint($request);
    }
    public function GetConfig(Request $request)
    {
        $this->jobMethod = 'GetConfig';
        return $this->handleCustomEndPoint($request);
    }
    public function adminView(){
        return view('admin');
    }
    public function configView(){
        return view('config');
    }
    public function renderShortUrl(RedirectRequest $request){
        $data = (new UrlRepository())->UrlDecode(['data'=>$request->route()->parameters()]);
        return Redirect::to($data['url']??'');
    }
}
