<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller;

class BaseController extends Controller
{
    protected $jobMethod;
    protected $repo;
    protected $repository;

    public function __construct()
    {
        if ($this->repository) {
            $this->repo = new $this->repository;
        }
    }
    public function handleCustomEndPoint($request, $additionalData = [])
    {
        $requestData = [];
        if ($request) {
            $requestData = array_replace_recursive(
                $request->all(),
                $request->route()->parameters(),
                $additionalData
            );
        }
        $data = $this->repo->{$this->jobMethod}(['data' => $requestData]) ?? [];
        return $this->standardResponse($data, $data['http_status_code'] ?? 200);
    }
    public function standardResponse($data, $httpCode = 200)
    {
        return response()->json([
            "data" => $data,
            "status" => $httpCode
        ], $httpCode);
    }
}
