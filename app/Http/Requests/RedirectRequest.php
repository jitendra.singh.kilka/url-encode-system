<?php

namespace App\Http\Requests;

use App\Models\Url;
use App\Services\UrlService;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Redirect;

class RedirectRequest extends FormRequest
{
    function rules()
    {
        $short_url = request('short_url');
        $configs = UrlService::getConfig();
        if(!(Url::where('short_url','=',$short_url)
        ->where('expriration_at','>=',Carbon::now()->format("Y-m-d H:i:s"))
        ->where('used_count','<',$configs['used_count_limit'])
        ->exists())){
            abort(404, 'Invalid Url');
        }
        return [];
    }
}
