<?php

namespace App\Http\Requests;

use App\Models\Url;
use App\Services\UrlService;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class DecodeRequest extends FormRequest
{
    function rules()
    {
        $rules =  [
            'short_url' => [
                'required',
                'string',
                function($a,$v,$f){
                    $configs = UrlService::getConfig();
                    if(!(Url::where('short_url','=',$v)
                    ->where('expriration_at','>=',Carbon::now()->format("Y-m-d H:i:s"))
                    ->where('used_count','<',$configs['used_count_limit'])
                    ->exists())){
                        $f('Invalid Url.');
                    }
                }
            ]
        ];
        return $rules;
    }
}
