<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EncodeRequest extends FormRequest
{
    function rules()
    {
        $rules =  [
            'url' => [
                'required',
                'string'
            ]
        ];
        return $rules;
    }
}
