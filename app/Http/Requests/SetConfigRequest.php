<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SetConfigRequest extends FormRequest
{
    function rules()
    {
        $rules =  [
            'expriration_days' => [
                'required',
                'numeric',
                'gte:1'
            ],
            'used_count_limit' => [
                'required',
                'numeric',
                'gte:1'
            ]
        ];
        return $rules;
    }
}
