<?php

use App\Http\Controllers\UrlEncodeController;
use Illuminate\Support\Facades\Route;

Route::middleware([
    //middlewares
])->group(function () {
    Route::post('/url-encode', [UrlEncodeController::class, 'UrlEncode']);
    Route::get('/url-decode', [UrlEncodeController::class, 'UrlDecode']);
    Route::post('/set-config', [UrlEncodeController::class, 'SetConfig']);
    Route::get('/get-config', [UrlEncodeController::class, 'GetConfig']);
});
