<?php

use App\Http\Controllers\UrlEncodeController;
use Illuminate\Support\Facades\Route;

Route::get('/admin', [UrlEncodeController::class, 'adminView']);
Route::get('/config', [UrlEncodeController::class, 'configView']);
Route::get('/url/{short_url}', [UrlEncodeController::class, 'renderShortUrl']);
