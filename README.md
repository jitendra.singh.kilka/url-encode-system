# Architecture

<a href="https://ibb.co/54knbSr"><img src="https://i.ibb.co/pwQRYMf/system.png" alt="system" border="0" height="500px" width="1100px"></a>

# Tech Stack
<ul>
    <li>Laravel</li>
    <li>Mongodb - <small>we don’t really need lots of relationship among data, but we do need the fast read and write speed.</small></li>
    <li>Redis - <small>todo.</small></li>
</ul>

# Api Documentation

### Encode Url

```
Url - http://15.206.75.100/api/url-encode
Method - POST

Post Data - 
{
    "url":"string"
}

Output - 
{
    "data": {
        "short_url": "string"
    },
    "status": number
}
```
### Decode Url

```
Url - http://15.206.75.100/api/url-decode?short_url=string
Method - GET

Output - 
{
    "data": {
        "url": "string"
    },
    "status": number
}
```

### Set Configration for Urls

```
Url - http://15.206.75.100/api/set-config
Method - POST

Post Data - 
{
    "expriration_days":number,
    "used_count_limit":number
}

Output - 
{
    "data": [],
    "status": number
}
```
### Get Configration of Urls

```
Url - http://15.206.75.100/api/get-config
Method - GET

Output - 
{
    "data": {
        "expriration_days":number,
        "used_count_limit":number
    },
    "status": number
}
```

# UI

### Encode Url
```
http://15.206.75.100/admin
```
### Update Configration Url
```
http://15.206.75.100/config
```
