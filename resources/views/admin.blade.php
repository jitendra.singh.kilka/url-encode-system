<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Admin</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

    <style>
        .margin-top-10{
            margin-top: 10%;
        }
        .margin-top-5{
            margin-top: 5%;
        }
    </style>
</head>
    <body class="antialiased">
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                  <a class="navbar-brand" href="/admin">Admin Panel</a>
                </div>
                <ul class="nav navbar-nav">
                  <li class="active"><a href="/admin">Home</a></li>
                </ul>
                <ul class="nav navbar-nav">
                  <li class="active"><a href="/config">Configration of Url</a></li>
                </ul>
              </div>
        </nav>
        <div class="container">
            <div class="raw">
                <div class="col-10">
                    <h2>Encode Url</h2>
                    <textarea class="form-control rounded-0 margin-top-5" id="url_for_encode" rows="3" placeholder="paste url here..."></textarea>
                    <button class="btn btn-success margin-top-5" onclick="create_url()">Submit</button>
                    <div id="short_url" class="margin-top-5"></div>
                </div>
          </div>
    </body>
</html>
<script>
    document.getElementById('short_url').style.display = 'none';
    function create_url(){
        let api_url = "{{url('/')}}/api/url-encode";
        let data = {};
        data.url = document.getElementById('url_for_encode').value;
        $.ajax({
            type: 'POST',
            url: api_url,
            dataType:'json',
            data : data,
            success: function (data) {
                let short_element = document.getElementById('short_url');
                short_element.innerHTML = "Short Url - <a href='{{url('/')}}/url/"+data.data.short_url+"'>{{url('/')}}/url/"+data.data.short_url+"</a>";
                short_element.style.display = 'block';
            },
            error: function (request,error) {
                alert(JSON.parse(request.responseText).message.url);
            }
        });
    }
</script>
