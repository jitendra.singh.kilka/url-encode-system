<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Admin</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

    <style>
        .margin-top-10{
            margin-top: 10%;
        }
        .margin-top-5{
            margin-top: 5%;
        }
    </style>
</head>
    <body class="antialiased">
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
              <div class="navbar-header">
                <a class="navbar-brand" href="/admin">Admin Panel</a>
              </div>
              <ul class="nav navbar-nav">
                <li class="active"><a href="/admin">Home</a></li>
              </ul>
              <ul class="nav navbar-nav">
                <li class="active"><a href="/config">Configration of Url</a></li>
              </ul>
            </div>
        </nav>
        <div class="container">
            <div class="raw">
                <div class="col-10">
                    <h2>Configration of Url</h2>
                    <div class="input-group margin-top-5">
                        <span class="input-group-addon">Exprired in Days</span>
                        <input id="expriration_days" type="number" class="form-control" name="expriration_days" placeholder="Exprired in Days">
                      </div>
                      <div class="input-group margin-top-5">
                        <span class="input-group-addon">Used Limit</span>
                        <input id="used_count_limit" type="number" class="form-control" name="used_count_limit" placeholder="Used Limit">
                      </div>
                    <button class="btn btn-success margin-top-5" onclick="create_url()">Submit</button>
                </div>
          </div>
    </body>
</html>
<script>
    update();
    function update(){
        let api_url = "{{url('/')}}/api/get-config";
        $.ajax({
            type: 'GET',
            url: api_url,
            success: function (data) {
                document.getElementById('expriration_days').value = data.data.expriration_days;
                document.getElementById('used_count_limit').value = data.data.used_count_limit;
            },
            error: function (request,error) {
            }
        });
    }
    function create_url(){
        let api_url = "{{url('/')}}/api/set-config";
        let data = {};
        data.expriration_days = parseInt(document.getElementById('expriration_days').value,10);
        data.used_count_limit = parseInt(document.getElementById('used_count_limit').value,10);
        $.ajax({
            type: 'POST',
            url: api_url,
            dataType:'json',
            data : data,
            success: function (data) {
                update();
                alert('configration updated');
            },
            error: function (request,error) {
                if(JSON.parse(request.responseText).message.expriration_days??false)
                alert(JSON.parse(request.responseText).message.expriration_days);
                if(JSON.parse(request.responseText).message.used_count_limit??false)
                alert(JSON.parse(request.responseText).message.used_count_limit);
            }
        });
    }
</script>
