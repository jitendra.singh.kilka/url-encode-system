# Deployment
## Laravel Api
### Requirements
<ul>
<li>Apache|Nginx|Any..</li>
<li>Composer</li>
<li>PHP &gt;= 7.2.5</li>
<li>BCMath PHP Extension</li>
<li>Ctype PHP Extension</li>
<li>Fileinfo PHP extension</li>
<li>JSON PHP Extension</li>
<li>Mbstring PHP Extension</li>
<li>OpenSSL PHP Extension</li>
<li>PDO PHP Extension</li>
<li>Tokenizer PHP Extension</li>
<li>XML PHP Extension</li>
<li>Mongodb PHP Extension</li>
</ul>

```
sudo apt install apache2
sudo apt install php libapache2-mod-php php-mbstring php-xmlrpc php-soap php-gd php-xml php-cli php-zip php-bcmath php-tokenizer php-dev php-json php-pear phpize
sudo pecl install mongodb
git clone https://gitlab.com/jitendra.singh.kilka/url-encode-system.git
composer install --optimize-autoloader --no-dev
```
### Laravel Setup With Apache
<p>create a new virtual host for the project. It can be done easily with the commands below:</p>

```
sudo nano /etc/apache2/sites-available/laravel.conf
```
<p>Add the following to create the new Virtual host. Remember to replace thedomain.com with your server’s IP address.</p>

```
<VirtualHost *:80>
   ServerName thedomain.com
   ServerAdmin webmaster@thedomain.com
   DocumentRoot /var/www/html/example/public

   <Directory /var/www/html/example>
       AllowOverride All
   </Directory>
   ErrorLog ${APACHE_LOG_DIR}/error.log
   CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
```
<p>enable the new virtual host:</p>

```
sudo a2dissite 000-default.conf
sudo a2ensite laravel_project
sudo a2enmod rewrite
sudo systemctl restart apache2
```
## Database
### Mongodb
```
curl -fsSL https://www.mongodb.org/static/pgp/server-4.4.asc | sudo apt-key add -
echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.4.list
sudo apt update
sudo apt install mongodb-org
sudo systemctl start mongod.service
sudo systemctl enable mongod

```
